defmodule Ecommerce.Api do
  @moduledoc """
  The Api context.
  """

  import Ecto.Query, warn: false

  alias Ecommerce.Repo
  alias Ecommerce.Api.Product

  def by_name(query, name) do
    from p in query,
    where: ilike(p.name, ^"%#{name}%")
  end

  def min_price(query, min_price) do
    from p in query,
    where: p.price >= ^min_price
  end

  def max_price(query, max_price) do
    from p in query,
    where: p.price <= ^max_price
  end

  def get_products_by([name: name, min_price: min_price, max_price: max_price]) do
    Product
    |> by_name(name)
    |> min_price(min_price)
    |> max_price(max_price)
    |> get_products()
  end

  def get_products_by([name: name, max_price: max_price]) do
    Product
    |> by_name(name)
    |> max_price(max_price)
    |> get_products()
  end

  def get_products_by([name: name, min_price: min_price]) do
    Product
    |> by_name(name)
    |> min_price(min_price)
    |> get_products()
  end

  def get_products_by([name: name]) do
    Product
    |> by_name(name)
    |> get_products()
  end

  def get_products(query), do: Repo.all(query)

  def create_products(products \\ %{}) do
    Repo.transaction(fn ->
      changesets = %Product{}
      |> Product.changests(products)

      for cs <- changesets do
        case Repo.insert(cs) do
          {:error, _} ->
            Repo.rollback(cs)
          _ -> IO.puts("ok")
        end
      end
    end)
  end

  def create_product(product \\ %{}) do
    %Product{}
    |> Product.changeset(product)
    |> Repo.insert()
  end
end
