defmodule Ecommerce.Date do
  @behaviour Ecto.Type

  use Timex

  @date_format "{0M}/{0D}/{YYYY}"

  def type(), do: :date

  def dump(term), do: {:ok, term}

  def load(term), do: {:ok, term}

  def cast(term) do
    with {:ok, datetime} <- Timex.parse(term, @date_format),
         date <- Timex.to_date(datetime)
    do
      {:ok, date}
    else
      _ -> :error
    end
  end

  def equal?(nil, _term), do: false

  def equal?(term1, term2) do
    case DateTime.compare(term1, term2) do
      :eq -> true
      _ -> false
    end
  end
end
