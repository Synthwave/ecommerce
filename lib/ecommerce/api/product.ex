defmodule Ecommerce.Api.Product do
  use Ecto.Schema
  import Ecto.Changeset

  @valid_product_name ~r/^\w[a-zA-Z\d\s\-]+$/

  schema "products" do
    field :name, :string
    field :price, :integer
    field :start_date, Ecommerce.Date

    timestamps()
  end

  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :price, :start_date])
    |> validate_required([:name, :price, :start_date])
    |> validate_length(:name, min: 4, max: 10)
    |> validate_format(:name, @valid_product_name)
    |> validate_number(:price, greater_than: 0)
    |> validate_date_is_today()
  end

  def changests(product, attrs) do
    for attr <- attrs do
      changeset(product, attr)
    end
  end

  defp validate_date_is_today(changeset) do
    validate_change(changeset, :start_date, fn _, start_date ->
      cond do
        Date.compare(start_date, Date.utc_today()) == :lt ->
          [start_date: "must today or a date in the future"]
        true -> []
      end
    end)
  end
end
