defmodule EcommerceWeb.ProductView do
  use EcommerceWeb, :view
  use Timex
  alias EcommerceWeb.ProductView

  def render("show.json", %{products: products}) do
    %{posts: render_many(products, ProductView, "product.json")}
  end

  def render("created.json", _params) do
    %{
       status: "ok",
       detail: "created"
    }
  end

  def render("product.json", %{product: product}) do
    %{
      name: product.name,
      price: product.price,
      start_date: product.start_date |> date_to_string_format()
    }
  end

  defp date_to_string_format(date) do
    # Assume, it is always correct.
    Timex.format!(date, "{0M}/{0D}/{YYYY}")
  end
end
