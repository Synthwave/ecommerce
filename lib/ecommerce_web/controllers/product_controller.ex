defmodule EcommerceWeb.ProductController do
  use EcommerceWeb, :controller

  alias Ecommerce.Api

  action_fallback EcommerceWeb.FallbackController

  def create(conn, %{"posts" => product_params}) when is_list(product_params) do
    case Api.create_products(product_params) do
      {:ok, _} ->
        conn
        |> put_status(:created)
        |> render("created.json")
      error -> error
    end
  end

  def show(conn, %{"keyword" => name, "min_price" => min_price, "max_price" => max_price}) do
    products = Api.get_products_by(
      name: name,
      min_price: min_price,
      max_price: max_price)
    render(conn, "show.json", products: products)
  end

  def show(conn, %{"keyword" => name, "max_price" => min_price}) do
    products = Api.get_products_by(
      name: name,
      max_price: min_price)
    render(conn, "show.json", products: products)
  end

  def show(conn, %{"keyword" => name, "min_price" => min_price}) do
    products = Api.get_products_by(
      name: name,
      min_price: min_price)
    render(conn, "show.json", products: products)
  end

  def show(conn, %{"keyword" => name}) do
    products = Api.get_products_by(name: name)
    render(conn, "show.json", products: products)
  end
end
