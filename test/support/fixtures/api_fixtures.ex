defmodule Ecommerce.ApiFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Ecommerce.Api` context.
  """

  @doc """
  Generate a product.
  """
  def product_fixture(attrs \\ %{}) do
    {:ok, product} =
      attrs
      |> Enum.into(%{
        name: "some name",
        price: 42,
        start_date: ~D[2022-02-03]
      })
      |> Ecommerce.Api.create_product()

    product
  end
end
