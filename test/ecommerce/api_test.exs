defmodule Ecommerce.ApiTest do
  use Ecommerce.DataCase

  alias Ecommerce.Api

  describe "products" do
    alias Ecommerce.Api.Product

    import Ecommerce.ApiFixtures

    @invalid_attrs %{name: nil, price: nil, start_date: nil}

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Api.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Api.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      valid_attrs = %{name: "some name", price: 42, start_date: ~D[2022-02-03]}

      assert {:ok, %Product{} = product} = Api.create_product(valid_attrs)
      assert product.name == "some name"
      assert product.price == 42
      assert product.start_date == ~D[2022-02-03]
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Api.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      update_attrs = %{name: "some updated name", price: 43, start_date: ~D[2022-02-04]}

      assert {:ok, %Product{} = product} = Api.update_product(product, update_attrs)
      assert product.name == "some updated name"
      assert product.price == 43
      assert product.start_date == ~D[2022-02-04]
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Api.update_product(product, @invalid_attrs)
      assert product == Api.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Api.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Api.get_product!(product.id) end
    end

    test "change_product/1 returns a product changeset" do
      product = product_fixture()
      assert %Ecto.Changeset{} = Api.change_product(product)
    end
  end
end
