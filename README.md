# Ecommerce

# Endpoints

POST /api/v1/products

```json

{
  "posts" [
    {
      "name": "item",
      "price": 42,
      "start_date": "01/01/2030"
    }
  ]
}
```

GET /api/v1/products/search&keyword=<somethinbg>?min_price=<integer>?max_price=<integer>

Where `min_price` and `max_price` are optional

## Production

Build a docker image and push into a registry

Edit `k8s/deployment.yaml`

Deploy in Kubernetes with the following command:

`kubectl apply -f k8s/deployment.yaml`

## Developmnet

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).
